package task1oop;

public class Main {

	public static void main(String[] args) {
		Car myCar = new Car("Audi");
		myCar.setColor("black");
		myCar.setPrice(999999999);	
		myCar.setId(1);
		myCar.setWarranty(5);
		
		System.out.println(myCar.getModel());
		System.out.println(myCar.getColor());
		System.out.println(myCar.getPrice());
		System.out.println(myCar.getId());
		System.out.println(myCar.getWarranty());
	}

}
